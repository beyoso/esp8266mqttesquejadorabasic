
///////Wifi
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>//V6.03
#include <Utils.h>
//////Arduino
#include <Arduino.h>

#include "DHT.h"
//PID
#include <PID_v1.h>

//Mosfet
#define mosfetOn 0
#define mosfetOff 1
const char mosfet1Humedad =  D5;
const char mosfet2Led =  D6;
const char mosfet3Ventilador =  D7;
const char mosfet4Resistencia =  D8;
//Rele
#define releOn 0 
#define releOff 1
const char rele = D3;

//DHT
const char pinDht = D2;
DHT dht;
float humAc = 0;
double temAc = 0;
float humOld = 0;
float temOld = 0;
int contDht = 0;
int periodoDht = 2000;
unsigned long tiempoDht;
int periodoEnvio = 20000;
unsigned long tiempoEnvio;
int periodoClima = 10000;
unsigned long tiempoClima;
unsigned long tiempoChangeLed;
int periodoLed = 5000;


//Temp&Hum
int humDeseada = 90;
int temDeseada = 25;
int stateLed = 0;
int mode =0 ;
int actLed = 0;
//PID
// Constantes del controlador
double Kp=2, Ki=5, Kd=1;
 
// variables externas del controlador
double  OutputTempC,OutputTempF ,Setpoint;
PID pidControllerTempC(&temAc, &OutputTempC, &Setpoint, Kp, Ki, Kd, DIRECT);
PID pidControllerTempF(&temAc, &OutputTempF, &Setpoint, Kp, Ki, Kd, DIRECT);
//ESP8266
// establecemos los parametros para tener acceso a la red
const char* ssid = "vereda-R";
const char* password = "vereda2019";
const char* mqtt_server = "192.168.1.100";
const int mqtt_port= 1883;

const char* topicTemp = "campo/esquejadora/temperatura";
const char* topicHum = "campo/esquejadora/humedad";
const char* topicLedEmiter = "campo/esquejadora/ledEmiter";
const char* topicrotacionH = "campo/esquejadora/rotacionH";
const char* topicSetValue = "campo/esquejadora/setValue";
const char* nameNode = "NodoEsquejadora";
//topics PIDs
const char* topicPid = "campo/esquejadora/PID";
WiFiClient espClient;
PubSubClient client(espClient);



void setup_wifi() 
{
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void controlClima(int tempActual, int tempDeseada, int humActual, int humDeseada)
{
  pidControllerTempC.Compute();
  analogWrite(mosfet4Resistencia, OutputTempC);
  pidControllerTempF.Compute();
  analogWrite(mosfet3Ventilador, OutputTempF);

    String payload ="{";payload += "\"pwmResistencia\":"; payload += OutputTempC; 
   payload += ",\"pwmVentilador\":"; payload += OutputTempF; payload += "}";
    char attributes[200];
    payload.toCharArray( attributes, 200 );
    client.publish(topicPid,attributes);
  /*
  //Temperatura
  if (tempActual < tempDeseada )
  {
    //frio
    pidControllerTempC.Compute();
    analogWrite(mosfet4Resistencia, OutputTempC);
  }else if (tempActual  > tempDeseada )
  {
    //Mucho calor
    digitalWrite(mosfet3Ventilador, mosfetOn);
  }else
  {
    digitalWrite(mosfet3Ventilador, mosfetOff);
    digitalWrite(mosfet4Resistencia, mosfetOff);
  }
  */
 //Humedad
 digitalWrite(mosfet1Humedad,(humActual < humDeseada - 5)?mosfetOn:mosfetOff);
    
  
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(nameNode)) {
      Serial.println("connected");
      // Enviamos los datos obtenidos
     client.publish(topicLedEmiter, nameNode);
     client.publish(topicHum, nameNode);
     client.publish(topicTemp, nameNode);
      // ... and resubscribe
      client.subscribe(topicSetValue);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setTempYHumedad()
{
  
  delay(dht.getMinimumSamplingPeriod());
  float h = dht.getHumidity();
  float t = dht.getTemperature();
  if(!(isnan(h)|| isnan(t)))
  {
    humAc += h;
    temAc += t;
    contDht++;
  }
  
  Serial.print("temperature: ");Serial.print(t);Serial.print(" humedad: ");Serial.println(h); 
  
}
void sendHumAndTemp(float hum, float temp)
{
    String payload ;
    payload ="{";payload += "\"temperature\":"; payload += temp; payload += "}";
    char attributes[200];
  
    payload.toCharArray( attributes, 200 );
    Serial.println( attributes );
        if (client.publish( topicTemp, attributes ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
    payload  ="{";payload += "\"humidity\":"; payload += hum; payload += "}";
    char attributes2[200];
  
    payload.toCharArray( attributes2, 200 );
    Serial.println( attributes2 );
        if (client.publish( topicHum, attributes2 ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
  
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message recibido [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  //JSON
  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, payload);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  mode = doc["mode"];
  //{"mode":0,"temperatura":25,"humedad":90,"led":1}
  //mode Auto = 0
  if(mode == 0){
  temDeseada = doc["temperatura"];
  humDeseada = doc["humedad"];
  stateLed = !doc["led"];
  
  
  //modo Manual mode != 0
  }  else{
    //{"mode":1,"releA":1,"releA":1,"humidificador":1,"ventilador":1,"led":1}
     
  digitalWrite(mosfet1Humedad ,!doc["humidificador"]);
  digitalWrite(mosfet3Ventilador,!doc["ventilador"]);
  stateLed = !doc["led"];
  }
  int auxR = doc["rele"];
  if(auxR!= 0)
  {
    digitalWrite(rele,!doc["rele"] );
  }

}

void ActualizarLed()
{
  char auxL= actLed;
  if(stateLed)
  {
     actLed++;
     actLed = actLed>100?100:actLed;
  }else
  {
    actLed--;
    actLed = actLed<0?0:actLed;
  }
  if(actLed != auxL){
    analogWrite(mosfet2Led, map(actLed,100,0,0,1024));
    String payload ;
    payload ="{";payload += "\"pwmLed\":"; payload += actLed; payload += "}";
    char attributes[200];
    payload.toCharArray( attributes, 200 );
    client.publish(topicLedEmiter,attributes);
  }

}
void setup() 
{
  //Serial
  Serial.begin(115200);
  //Mosfet
  
  pinMode(mosfet1Humedad, OUTPUT);  //humificador 
  pinMode(mosfet2Led, OUTPUT);         //LED
  pinMode(mosfet3Ventilador, OUTPUT);//Ventiladores
  pinMode(mosfet4Resistencia,OUTPUT);
  digitalWrite(mosfet1Humedad, mosfetOff);
  digitalWrite(mosfet2Led, mosfetOff);
  digitalWrite(mosfet3Ventilador, mosfetOff);
  digitalWrite(mosfet4Resistencia,mosfetOff);
  //reles
  pinMode(rele, OUTPUT);       //Rele 
  digitalWrite(rele,releOff);
 
  //DHT 
  dht.setup(pinDht);
  tiempoDht = millis();
  tiempoEnvio = millis();
  tiempoClima = millis();
  tiempoChangeLed = millis();
  //Wifi
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  //PID
  Setpoint = 100;
  pidControllerTempC.SetMode(AUTOMATIC);
  pidControllerTempF.SetMode(AUTOMATIC);
}

void loop() 
{
  
  if(!WiFi.isConnected())
  {setup_wifi();}
  if (!client.connected()) 
  {reconnect();}

  client.loop();
  //led
  if(millis()>tiempoChangeLed+periodoLed){
    tiempoChangeLed = millis();
    ActualizarLed();
  }

  //
  //DHT
  if(millis()>tiempoDht +periodoDht)
  {
    tiempoDht = millis();
    setTempYHumedad();
  }
  //Clima
  if(millis()>tiempoEnvio+periodoEnvio)
  {
    tiempoEnvio = millis();
    humAc = (humAc/contDht);
    temAc = (temAc/contDht);
    if(temAc != temOld || humAc != humOld )
    {
      temOld = temAc;
      humOld = humAc; 
      sendHumAndTemp(humAc,temAc);
      if(mode == 0)
      controlClima(temAc,temDeseada,humAc,humDeseada);
    }
    humAc = 0;
    temAc =0;
    contDht = 0;
  }
}



